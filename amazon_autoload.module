<?php 

/**
 * Implements amazon_item_clean_xml.
 * 
 * @param $item The items array to store data
 * @param $xml The xml returned by the Amazon Web Service
 * 
 * Returns nothing
 */
function amazon_autoload_amazon_item_clean_xml($item, $xml) {
  // Handle the similar products
  if (isset($xml->SimilarProducts)) {
    foreach ($xml->SimilarProducts->SimilarProduct as $data) {
      $title = (string) $data->Title;
      $asin  = (string) $data->ASIN;
      $item['similarproduct'][] = array(
        'title' => $title,
        'asin' => $asin,
      );
      watchdog('amazon_autoload', (string)$data->Title);
      
      // if item does not yet exist add it to the queue of items to insert
      $result = db_query("SELECT field_amazon_item_asin FROM {field_data_field_amazon_item} WHERE field_amazon_item_asin=:asin", array(':asin' => $asin));
      if($result->rowCount() == 0) {     
        db_merge('amazon_autoload')
  		  ->key(array('asin' => $asin))
  		  ->fields(array(
  		    'title' => $title,
  		    'last_updated' => time(),
  		  ))
          ->execute();
      };
    };
  };
};

/**
 * Implements hook_schema.
 */
function amazon_autoload_schema() {
  $schema['amazon_autoload'] = array(
    'description' => 'TODO: please describe this table!',
    'fields' => array(
      'title' => array(
        'description' => 'The title of the item',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
      ),
      'asin' => array(
        'description' => 'The asin of the item',
        'type' => 'varchar',
        'length' => '16',
        'not null' => TRUE,
        'default' => '',
      ),
      'last_updated' => array(
        'description' => 'the timestamp this record was last updated',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('asin'),
  );
	
  return $schema;
};

// Hook cron
// Insert the items into amazon whatever.
function amazon_autoload_cron() {
  // get an item from the database
  $result = db_query('SELECT * FROM {amazon_autoload} ORDER BY last_updated ASC');
  $record = $result->fetchAssoc();
  watchdog('amazon_autoload', "Got " . $record['title'] . " with asin " . $record['asin']);    
  
  // insert into the database
  $node = new StdClass();
  $node->type = 'amazon_node';
  $node->language = LANGUAGE_NONE;
  node_object_prepare($node);
  $node->title = $record['title'];
  $node->body[$node->language][0]['format']  = 'filtered_html';
  $node->field_amazon_item[$node->language][0]['asin'] = $record['asin'];
  
  node_save($node);
  
  // remove item from autoload table
  db_delete('amazon_autoload')
    ->condition('asin', $record['asin'])
    ->execute();
}